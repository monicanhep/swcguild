function doLoadStuff() {
    document.getElementById("showme").style.display = "none";
 }

function roll() {
    var num = parseInt(document.getElementById("num").value);
    var bet = num;
    var totalmoneywon = bet;
    var rollcounter = 0;
    var totalhighestroll = 0;
    var totalrollssofar = 0;
    var output = document.getElementById("output");
    var numberofrolls = document.getElementById("numberofrolls");
    var moneywon = document.getElementById("moneywon");
    var highestroll = document.getElementById("highestroll");
    var playButton = document.getElementById("playButton");
  
    if (bet < 0 || bet ==0) {
        alert("Must bet at least $1!");
    }
    else if (isNaN(bet)) {
        alert("Gotta pay if you wanna play");
    }
    else if (bet >100) {
        alert("Save your money, bet must be under $100");
    }
    else {
        while (bet > 0) {
            d1 = Math.floor(Math.random() * 6) + 1;
            d2 = Math.floor(Math.random() * 6) + 1;
            diceTotal = d1 + d2;
            console.log("dice#1 =" + d1 + "dice#2 =" + d2 + "total roll=" + diceTotal);
            totalrollssofar++;

            if (diceTotal == 7) {
                bet += 4;
                rollcounter++;
                console.log("won 4 dollars");
            }

            if (bet > totalmoneywon) {
                totalmoneywon = bet;
                totalhighestroll = totalrollssofar;
            }
            else {
                bet -= 1;
                rollcounter = 0;
            }
        }
        function results() {
            document.getElementById("showme").style.display = "";
            document.getElementById("output").innerHTML = "" + num;
            document.getElementById("numberofrolls").innerHTML = totalrollssofar + " ";
            document.getElementById("moneywon").innerHTML = " " + totalmoneywon;
            document.getElementById("highestroll").innerHTML = " " + totalhighestroll;
            document.getElementById("button1").innerHTML = "Play Again";
           
        }
        results();
    }
}